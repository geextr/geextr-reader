import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CardComponent} from "./card/card.component";
import {RedirectComponent} from "./redirect/redirect.component";
import {UserComponent} from "./user/user.component";

const routes: Routes = [
  {path: 'reader/:id', component: CardComponent},
  {path: 'user/:action', component: UserComponent},
  {path: 'redirect', component: RedirectComponent},
  {path: '**', redirectTo: 'redirect'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
