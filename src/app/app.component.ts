import {Component, OnInit} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

@Component({
  selector: 'app-root',
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public showHeader = true;

  private excludedRoutes = [
    '/user/auth'
  ];

  constructor(location: Location) {
    var route = location.path();
    if (this.excludedRoutes.includes(route)) {
      this.showHeader = false;
    }
  }

}
