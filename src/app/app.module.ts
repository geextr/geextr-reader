import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CardComponent } from './card/card.component';
import { HeaderComponent } from './header/header.component';

import { NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { SliderComponent } from './slider/slider.component';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { RedirectComponent } from "./redirect/redirect.component";
import { UserComponent } from './user/user.component';
import { UserSigninComponent } from './user-signin/user-signin.component';
import { UserAuthComponent } from './user-auth/user-auth.component';
import {NgxUiLoaderModule} from "ngx-ui-loader";
import { ModalWindowComponent } from './modal-window/modal-window.component';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    HeaderComponent,
    SliderComponent,
    RedirectComponent,
    UserComponent,
    UserSigninComponent,
    UserAuthComponent,
    ModalWindowComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxUiLoaderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
