import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  cardTitle = 'Witchblade';
  description = '';

  id: string;
  title  = '';

  private available_permissions = [
    'author',
    'reader'
  ];
  private permission_type = null;

  private routeSubscription: Subscription;

  constructor(private http: HttpClient, private route:ActivatedRoute, private router:Router) {
    //получаем id сущности
    this.routeSubscription = route.params.subscribe(params => this.id = params['id']);
  }

  ngOnInit() {
    if (this.id !== undefined) {
      //TODO Далее:
      // Через сервис.
      // 1) Получаем текущего пользователя и его права (автор, читатель, оператор(?))
      // 2) Получаем сущность для отображения
      //  а) Обложка
      //  б) Название
      //  в)описание
      //  г) список глав (если есть)
      //  д) список закладок (если есть) 3) Посылаем запрос на api для кэширования (продумать этот момент)
      if (isNaN(parseInt(this.id))) {
        this.router.navigate(['redirect']);
      }
      this.http.post('/library/reader/permissions', [{
        user: 0
      }]).subscribe(response => {
        this.permission_type = response['permission_type'];
        console.log('permissions: ' + this.permission_type);

      });

    }
    this.description = 'Рыбное описание: Равным образом сложившаяся ' +
      'структура организации обеспечивает широкому кругу (специалистов) участие в формировании форм развития. ' +
      'Идейные соображения высшего порядка, а также рамки и место обучения кадров играет важную роль ' +
      'в формировании существенных финансовых и административных условий. Разнообразный и богатый опыт консультация ' +
      'с широким активом обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых участниками ' +
      'в отношении поставленных задач. Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий ' +
      'обеспечивает широкому кругу (специалистов) участие в формировании существенных финансовых и административных условий. ';
  }

}
