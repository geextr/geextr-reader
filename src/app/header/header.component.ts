import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  //TODO имя пользователя, получаемое с сервера
  private nickname = '';
  private url = '';
  public showHeader = true;

  private excludedRoutes = [
    '/user/auth'
  ];

  constructor() {

  }

  ngOnInit() {
    this.nickname = 'niveus.noctua';
  }

}
