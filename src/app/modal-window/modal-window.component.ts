import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent implements OnInit {

  @Input() header: string;
  @Input() description: string;
  @Output() isConfirmed: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  private confirm() {
    this.isConfirmed.emit(true);
  }
  private close() {
    this.isConfirmed.emit(false);
  }

  ngOnInit() {
  }

}
