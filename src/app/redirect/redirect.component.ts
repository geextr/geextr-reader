import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-redirect',
  template: ``,
  styles: []
})
export class RedirectComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    //Компонент вызывается если url не совпадает ни с одним роутом
    //Если пользователь залогинен (проверям в бэке из сессии) - роутим в библиотеку
    //Если пользователь не залогинен - роутим на страницу регистрации
    let activeUser = false;
    //TODO здесь идет запрос к приложению user для проверки, залогинен ли пользователь
    if (!activeUser) {
      window.location.href = '/user/auth';
    }
  }

}
