import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { NgxUiLoaderService } from 'ngx-ui-loader';
import {Key} from "@ng-bootstrap/ng-bootstrap/util/key";
import {isEmpty} from "rxjs/operators";
import {UserStandartAuthorization} from "../user/entity/user.standart.authorization";
import {UserStandartRegistration} from "../user/entity/user.standart.registration";

@Component({
  selector: 'app-user-auth',
  templateUrl: './user-auth.component.html',
  styleUrls: ['./user-auth.component.scss']
})
export class UserAuthComponent implements OnInit {

  private signupForm: FormGroup;
  private signinForm: FormGroup;
  private httpClient: HttpClient;

  private url = 'http://geexter.back.localhost';
  private registration_type = 'registration_standart';
  private authorization_type = 'authorization_standart';

  private ngxService;

  private isShowModal = false;

  private response = null;

  private header = 'Test header';
  private body   = 'Test Body';

  public violations = [];

  registrationModel   = new UserStandartRegistration('', '', '');
  authorizationModel  = new UserStandartAuthorization('', '', '');

  constructor(httpClient: HttpClient, fb: FormBuilder, ngxService: NgxUiLoaderService) {
    this.httpClient = httpClient;
    this.ngxService = ngxService;
  }

  ngOnInit() {
    var reg  = document.getElementById('registration_form');
    var auth = document.getElementById('authorization_form');
    var add = document.getElementById('additional');
    reg.classList.add('material-card--center');
    auth.classList.add('material-card--right');
    add.classList.add('material-card--left');
    this.createForms();
  }


  createForms() {

    this.signupForm = new FormGroup({
      email: new FormControl(),
      login: new FormControl(),
      password: new FormControl(),
      registration_type: new FormControl()
    });

    this.signinForm = new FormGroup({
      email: new FormControl(),
      login: new FormControl(),
      password: new FormControl()
    })
  }

  submitSignup(ngxService: NgxUiLoaderService, context) {
    var formData: any = new FormData();
    formData.append("email", this.registrationModel.email);
    formData.append("login", this.registrationModel.login);
    formData.append("password", this.registrationModel.password);
    formData.append("registration_type", this.registration_type);

    ngxService.start();

    //создать Injectable сервис и брать url оттуда
    this.httpClient.post(this.url + '/user/signup', formData, {withCredentials: true}).subscribe(
      function (response) {
        console.log(response);
        context.processResponse(response, context);
        context.getActiveMessage();
        context.showDialog();
        ngxService.stop();
      },
      (error) => console.log(error)
    )
  }

  submitSignin(ngxService: NgxUiLoaderService, context) {
    var formData: any = new FormData();
    formData.append("email", this.authorizationModel.email);
    formData.append("login", this.authorizationModel.login);
    formData.append("password", this.authorizationModel.password);
    formData.append("authorization_type", this.authorization_type);

    ngxService.start();

    //создать Injectable сервис и брать url оттуда
    this.httpClient.post(this.url + '/user/signin', formData, {withCredentials: true}).subscribe(
      function (response) {
        context.processResponse(response, context);
        context.getActiveMessage();
        context.showDialog();
        ngxService.stop();
      },
      (error) => console.log(error)
    )
  }

  public showDialog() {
    this.isShowModal = true;
  }

  public processResponse(response, context) {
    var violations = response.violations;
    var success_messages = response.success_messages;
    violations.forEach(function (violation) {
      violation = violation['violation'];
        var violationKeys = Object.keys(violation);
        violationKeys.forEach(function (key) {
          var message = violation[key];
          context.violations.push(message);
        })
    });
  }

  public getActiveMessage() {
    if (this.violations.length > 0) {
      var message = this.violations.pop();
      this.header = message.title;
      this.body   = message.body;
    }
  }

  public closeDialog(isConfirmed) {
    if (!isConfirmed) {

    }
    this.isShowModal = false;
  }

  public signInForm() {

    var reg  = document.getElementById('registration_form');
    var auth = document.getElementById('authorization_form');
    var add = document.getElementById('additional');

    reg.classList.remove('material-card--center');
    reg.classList.add('material-card--left');

    auth.classList.remove('material-card--right');
    auth.classList.add('material-card--center');

    add.classList.add('material-card--left');
    add.classList.add('material-card--right');

    var inputs = reg.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];
      input.setAttribute('disabled', 'disabled');
      if (input.type == 'submit') {
        input.setAttribute('type', 'hidden');
      }
    }
  }

  public signUpForm() {

    var reg  = document.getElementById('registration_form');
    var auth = document.getElementById('authorization_form');
    var add = document.getElementById('additional');

    reg.classList.remove('material-card--left');
    reg.classList.add('material-card--center');

    auth.classList.remove('material-card--center');
    auth.classList.add('material-card--right');

    add.classList.remove('material-card--right');
    add.classList.add('material-card--left');

    var inputs = reg.getElementsByTagName("input");
    for (var i = 0; i < inputs.length; i++) {
      var input = inputs[i];
      input.removeAttribute('disabled');
      if (input.type == 'hidden') {
        input.setAttribute('type', 'submit');
      }
    }
  }

}
