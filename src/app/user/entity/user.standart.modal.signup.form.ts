export class UserStandartModalSignupForm {

  constructor(private email, private login, private password) {}

  public getEmail() {
    return this.email;
  }

  public getLogin() {
    return this.login;
  }

  public getPassword() {
    return this.password;
  }
}
