import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  private action = 'signup';

  constructor(private route:ActivatedRoute) {}

  ngOnInit() {
    this.action = this.route.snapshot.params.action;
  }

}
